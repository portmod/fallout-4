# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir

from common.fallout_4 import Fallout4
from common.nexus import NexusMod


class Package(Fallout4, NexusMod):
    NAME = "Fallout 3 10mm SMG"
    DESC = "10mm SMG from Fallout 3/New Vegas"
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/46195"
    NEXUS_URL = HOMEPAGE
    LICENSE = "all-rights-reserved"
    KEYWORDS = "~fallout-4"
    TEXTURE_SIZES = "4096 2048"
    SRC_URI = """
        texture_size_2048? ( 10mm_SMG_BA2_Version-46195-1-4-1595836628.7z )
        texture_size_4096? ( 10mm_SMG-46195-1-4-1595487956.7z 4k_Textures-46195-1-0-1594630666.7z )
    """
    INSTALL_DIRS = [
        # 2k textures
        InstallDir(
            ".",
            S="10mm_SMG_BA2_Version-46195-1-4-1595836628",
            PLUGINS=[File("F4CW10mmSMG.esp")],
            REQUIRED_USE="texture_size_2048",
        ),
        # 4k textures
        InstallDir(
            "Data",
            S="10mm_SMG-46195-1-4-1595487956",
            PLUGINS=[File("F4CW10mmSMG.esp")],
            REQUIRED_USE="texture_size_4096",
        ),
        InstallDir(
            "Data",
            S="4k_Textures-46195-1-0-1594630666",
            REQUIRED_USE="texture_size_4096",
        ),
    ]
