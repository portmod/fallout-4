# Copyright Copyright 2022-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir

from common.fallout_4 import Fallout4
from common.nexus import NexusMod


class Package(NexusMod, Fallout4):
    NAME = "Handmade Revolver"
    DESC = 'Adds a Handmade Revolver based on concept-art from the "The Art of Fallout 4" book.'
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/18457"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "~fallout-4"
    IUSE = "replacer no-leveled-list 2x-dmg 1-5x-dmg alt-sound"
    REQUIRED_USE = """
        ^^ ( replacer no-leveled-list )
        ?? ( 2x-dmg 1-5x-dmg )
    """
    NEXUS_SRC_URI = """
        https://nexusmods.com/fallout4/mods/18457?tab=files&file_id=75663 -> Handmade_Revolver-18457-1-6.rar
        alt-sound? ( https://www.nexusmods.com/fallout4/mods/43875?tab=files&file_id=177517 -> AsXas'_Handmade_Revolver_New_Sound-43875-1-0-1584215326.zip )
    """
    TEXTURE_SIZES = "1024 2048"
    INSTALL_DIRS = [
        InstallDir(
            "MAIN",
            S="Handmade_Revolver-18457-1-6",
        ),
        InstallDir(
            "MAIN-ESP",
            S="Handmade_Revolver-18457-1-6",
            PLUGINS=[File("HandmadeRevolver.esp")],
            REQUIRED_USE="!replacer",
        ),
        InstallDir(
            "REPLACER-ESP",
            S="Handmade_Revolver-18457-1-6",
            PLUGINS=[File("HandmadeRevolver.esp")],
            REQUIRED_USE="replacer",
        ),
        InstallDir(
            "NOLL-ESP",
            S="Handmade_Revolver-18457-1-6",
            PLUGINS=[File("HandmadeRevolver.esp")],
            REQUIRED_USE="no-leveled-list",
        ),
        InstallDir(
            "HQTEXTURES",
            S="Handmade_Revolver-18457-1-6",
            REQUIRED_USE="texture_size_2048",
        ),
        InstallDir(
            "STTEXTURES",
            S="Handmade_Revolver-18457-1-6",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "DMG-ESP",
            S="Handmade_Revolver-18457-1-6",
            PLUGINS=[
                File("HandmadeRevolver-2xDMG.esp", REQUIRED_USE="2x-dmg"),
                File("HandmadeRevolver-1.5xDMG.esp", REQUIRED_USE="1-5x-dmg"),
            ],
        ),
        InstallDir(
            ".",
            S="AsXas'_Handmade_Revolver_New_Sound-43875-1-0-1584215326",
            REQUIRED_USE="alt-sound",
        ),
    ]
