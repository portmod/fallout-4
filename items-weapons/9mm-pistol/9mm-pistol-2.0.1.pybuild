# Copyright Copyright 2022-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir

from common.fallout_4 import Fallout4
from common.nexus import NexusMod


class Package(NexusMod, Fallout4):
    NAME = "9mm Pistol (Browning Hi-Power) Redux"
    DESC = "Adds a classic 9mm Pistol from FNV"
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/20143"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "~fallout-4"
    IUSE = "tactical-reload"
    NEXUS_SRC_URI = """
        https://nexusmods.com/fallout4/mods/20143?tab=files&file_id=101166 -> 9mm_Pistol_(Browning_Hi-Power)-20143-2-0.zip
        https://nexusmods.com/fallout4/mods/20143?tab=files&file_id=102453 -> 9mm_Pistol_(Browning_Hi-Power)_Update-20143-2-0-1.zip
        https://www.nexusmods.com/fallout4/mods/57361?tab=files&file_id=228751 -> 9mm_Pistol_-_Crash's_Unofficial_Update-57361-1-2-1644121178.zip
        tactical-reload? ( https://www.nexusmods.com/fallout4/mods/52619?tab=files&file_id=215161 -> 9mm_Pistol_-_Tactical_Reload_Patch-52619-1-7-1629514887.rar )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data",
            S="9mm_Pistol_(Browning_Hi-Power)-20143-2-0",
        ),
        InstallDir(
            "Data",
            S="9mm_Pistol_(Browning_Hi-Power)_Update-20143-2-0-1",
            PLUGINS=[File("9mmPistol.esp")],
        ),
        InstallDir(
            ".",
            S="9mm_Pistol_-_Crash's_Unofficial_Update-57361-1-2-1644121178",
            PLUGINS=[File("9mmPistol-UnofficialUpdate.esp")],
        ),
        InstallDir(
            ".",
            S="9mm_Pistol_-_Tactical_Reload_Patch-52619-1-7-1629514887",
            PLUGINS=[File("9mmPistol_TacticalReloadPatch.esp")],
            REQUIRED_USE="tactical-reload",
        ),
    ]
