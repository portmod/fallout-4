# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Previsibines Repair Pack"
    DESC = "Previsibines performance upgrade package for Fallout 4, covering the entire game"
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/46403"
    NEXUS_URL = HOMEPAGE
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/unofficial-patch"
    TIER = "z"
    KEYWORDS = "~fallout-4"
    IUSE = "interiors-enhanced clarity point-lookout"
    SRC_URI = """
        PRP_Full_Stable-46403-0-59-5-1651043258.7z
        point-lookout? ( PRP_Point_Lookout_Compat-46403-0-59-1-1652424894.7z )
    """
    INSTALL_DIRS = [
        InstallDir(
            "02 Universal",
            S="PRP_Full_Stable-46403-0-59-5-1651043258",
            BLACKLIST=["AlootHomePlate*"],
        ),
        # There are plugins for other languages, if that's something that's wanted in the future
        InstallDir(
            "01 English",
            S="PRP_Full_Stable-46403-0-59-5-1651043258",
            PLUGINS=[
                File("PPF.esm"),
                File("PRP.esp"),
                File("PRP-Compat-IntEnh.esp", REQUIRED_USE="interiors-enhanced"),
                File("PRP-Compat-Clarity-42.esp", REQUIRE_USE="clarity"),
            ],
            BLACKLIST=[
                "AlootHomePlate.esp",
                "PRP-Compat-Clarity-43.esp",
                "PRP-Compat-DCA.esp",
                "PRP-Compat-Fogout-Int.esp",
                "PRP-Compat-RegionNames.esp",
                "PRP-Compat-UEL.esp",
                "PRP-Compat-UIL-Darker.esp",
                "PRP-Compat-UIL.esp",
            ],
        ),
        InstallDir(
            ".",
            S="PRP_Point_Lookout_Compat-46403-0-59-1-1652424894",
            PLUGINS=[File("PRP-Compat-PointLookout.esp")],
            REQUIRED_USE="point-lookout",
        ),
    ]
